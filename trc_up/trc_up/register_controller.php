<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 27/08/2015
 * Time: 4:57 PM
 */


class RegisterController extends AppController
{

    var $name = 'Register';
    var $uses = array('Register','User');

    function beforeFilter()
    {
        parent::beforeFilter();
        $this->layout = 'login';
        $this->Auth->allow(array('register','savedata','check_name'));
    }
    function register()
    {

    }
    function savedata(){
        if (!empty($this->data)) {
            if ($this->Register->save($this->data)) {
                $this->Session->setFlash('Registration completed.', true);
                $this->redirect(array('action' => 'register'));
            } else {
                $this->Session->setFlash(__('Registration can\'t be done .', true));

            }
        }
    }
    function check_name() {
        $this->layout = 'ajax';
        $db = ConnectionManager::getDataSource("default"); // name of your database connection
        //$res = "SELECT * FROM registration  where username='$name'";
        $users = $this->Register->find('all', array('conditions'=>array('name'=>$this->RequestHandler->params['form']['name'])));
        $numUsers = sizeof($users);
        if($numUsers>0) {
            $check= "Already Exists !";
        }
        else
            $check="Available";

            $this->set(compact('check'));
    }

}