
<div class="services index">

	<table cellpadding="0" cellspacing="0">
        <tr>
                <?php if($role=='Admin'){?>
                <th><?php echo $this->Paginator->sort('Sent to','to_user_id');?></th>
                <?php } ?>
                <th><?php echo $this->Paginator->sort('Subject','subject');?></th>
                <th><?php echo $this->Paginator->sort('Notice','notice');?></th>
                <th><?php echo $this->Paginator->sort('Sent On','sent_on');?></th>
                <th class="actions"><?php __('Actions');?></th>
        </tr>
	<?php
	$i = 0;
	foreach ($notices as $user):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>

        <?php if($role=='Admin'){?>
               <td><?php if($user['User']['name']) echo $user['User']['name']; else echo "All";?>&nbsp;</td>
        <?php } ?>
        <td><?php echo substr($user['Notice']['notice'],0,30).'....'; ?>&nbsp;</td>
        <td><?php echo $user['Notice']['subject']; ?>&nbsp;</td>
		<td><?php echo $user['Notice']['sent_on']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => ' ', $user['Notice']['id'])); ?>

		</td>
	</tr>
<?php endforeach; ?>
	</table>


	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>

  <?php if($role=='Admin'){?>
<div class="actions">

	<ul>
		<li><?php echo $this->Html->link(__('Send New Notice', true), array('controller' => 'Notice', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php } ?>