<div class="services form">
<?php echo $this->Form->create(null, array('url' => array('controller' => 'ProvisionalDiagnosis', 'action' => 'edit')));?>
	<fieldset>
		<legend><?php __('Edit Provisional Diagnosis'); ?></legend>
	<?php
		echo $this->Form->input('disease_name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>