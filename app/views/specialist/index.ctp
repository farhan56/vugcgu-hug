<div class="services index">

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('Specialization Type','type');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($specialists as $specialist):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>

		<td><?php echo $specialist['Specialist']['type']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $specialist['Specialist']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $specialist['Specialist']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $specialist['Specialist']['type'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>


	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>


<div class="actions">

	<ul>
		<li><?php echo $this->Html->link(__('Add Specialist', true), array('controller' => 'Specialist', 'action' => 'add')); ?> </li>
	</ul>
</div>