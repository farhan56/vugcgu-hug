<div class="services form">
<?php echo $this->Form->create(null, array('url' => array('controller' => 'Specialist', 'action' => 'add')));?>
	<fieldset>
		<legend><?php __('Add Specialist'); ?></legend>
	<?php
		echo $this->Form->input('type',array('label'=>'Specialization Type'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>