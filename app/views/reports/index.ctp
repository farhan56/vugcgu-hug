


<div class="services index">

	<table cellpadding="0" cellspacing="0">
        <tr>

                <th><?php echo $this->Paginator->sort('Date','to_user_id');?></th>
                <th><?php echo $this->Paginator->sort('Total Call','message');?></th>
                <th><?php echo $this->Paginator->sort('Answered Call','sent_on');?></th>
                <th><?php echo $this->Paginator->sort('Completed CRM','sent_on');?></th>
                <th><?php echo $this->Paginator->sort('Alarm Generated','sent_on');?></th>
        </tr>
	</table>


	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
