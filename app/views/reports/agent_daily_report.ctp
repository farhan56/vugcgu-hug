<style>

.agent_detail{
    cursor:pointer;
}
.white_content {
    	display: none;
    	position: absolute;
    	/*
    	top: 25%;
    	left: 25%;
    	*/
    	width: 37%;
    	height: 30%;
    	padding: 16px;
    	border: 16px solid orange;
    	background-color: white;
    	opacity:.95;
    	z-index:1002;
    	overflow: auto;
    }
</style>

<script type="text/javascript">

$(document).ready(function(){
     $('.agent_detail').click(function(){

        console.log($(this).attr('date')+' '+$(this).attr('agent_id'));
        $('#light').css('display','block');

        $.ajax({
            'url':'<?php echo $this->webroot; ?>/Reports/get_agent_detail',
            'type':'POST',
            'data':{'date':$(this).attr('date'),'agent_id':$(this).attr('agent_id')},
            'success':function(data){
                 $('#agent_log').html(data);
            },
            'beforeSend':function(){
                $('#agent_log').html('<h1>Loading...</h1>');
            }

        })

     });

});

</script>

<div class="cdrsearch">
<h2 style="font-family: l;"> Performance Report:: <?php echo '<span style="color:green; font-style:italic;"> From ' . $from_year . ' To ' . $to_year . '</span>' ;?></h2><br/>
<?php
    $role = $this->Session->read('role');
    $login_id = $this->Session->read('login_id');
	date_default_timezone_set('Asia/Dhaka');
	$today = date('Y-m-d') ;
	echo $form->create('Reports', array(
		'url' => array_merge(array('action' => 'agent_daily_report'), $this->params['pass'])
		));

   	echo $datePicker->picker('From');
	echo $datePicker->picker('To');

	if($role == 'Doctor' || $role == 'Agent'){
	    echo $form->input('agent_id',array('type'=>'text','readonly'=>'readonly','value'=>$login_id));
	}else{
	    echo $form->input('agent_id',array('type'=>'text'));
	}

	echo $form->submit(__('Show', true), array('name' => 'btnSubmit','div' => true));
	echo $form->end();
?>
<div class="clr"></div>
</div>
<div class="services index">

    	<br/>

    	<table  border=".5" cellpadding="0" cellspacing="0">

    		<tr>
    		    <th>Date</th>
    			<th>Agent</th>
    			<th>Login </th>
                <th>Logout </th>
                <th>Office Time</th>
                <th>Working Hour</th>
                <th>Offline Hour </th>
    			<th>Call Landed</th>
    			<th>Call Taken</th>
                <th>Alarm</th>
                <th>Talk Time</th>

    		</tr>
    		<div id="light" class="white_content">
                    <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" style="float:right">Close</a>
                    <div id="agent_log">
                    </div>
            </div>
    	<?php


    		$loop	=	0 ;
    		if(!isset($agentId)){$agentId=array();}
    		while( $loop < count($agentId) )
    		{ ?>

    			<tr class="agent_detail" date = '<?php echo$date[$loop] ?>' agent_id = '<?php echo $agentId[$loop]?>'>
    			<?php
    			    echo '<td>' ;
                    	echo $date[$loop] ;
                    echo '</td>';
    				echo '<td>' ;
    					echo $agentId[$loop] ;
    				echo '</td>';
    				echo '<td>' ;
                        echo $loginTime[$loop];
                    echo '</td>';
                    echo '<td>' ;
                        echo $logoutTime[$loop];
                    echo '</td>';
                    echo '<td>' ;
                        echo $officeTime[$loop];
                    echo '</td>';
                    echo '<td>' ;
                        echo $workingHour[$loop];
                    echo '</td>';
                    echo '<td>' ;
                        echo $offlineTime[$loop];
                    echo '</td>';
    				echo '<td>' ;
    					echo $callsLanded[$loop] ;
    				echo '</td>';
    				echo '<td>' ;
    					echo $callsTaken[$loop] ;
    				echo '</td>';
                    echo '<td>' ;
    					echo $alarms[$loop];
    				echo '</td>';
                    echo '<td>' ;
    					echo $talkTime[$loop] ;
    				echo '</td>';


    			echo '</tr>' ;

    			$loop++ ;
    		}

    	?>

        </table>
</div>
