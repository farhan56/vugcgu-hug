<div class="services form">

<?php echo $this->Form->create('User', array('type' => 'file','url' => array('controller' => 'User', 'action' => 'edit')));?>
	<fieldset>
		<legend><?php __('Edit User'); ?></legend>


	    <?php
            $url = $photo;
            echo '<div class="uploaded_image">';?>

            <img src="<?php echo $url;?>" alt="No Image Available" height="100" width="100"/>
            <?php echo '</div>';
            ?>
        <?php
	    echo $this->Form->input('photo', array('label'=>'Upload Photo (Must be<2MB(.png,.jpg))','type' => 'file'));
	    echo $this->Form->input('login_id',array('label'=>'Login','type'=>'text'));
	    echo $this->Form->input('name');
        echo $this->Form->input('password',array('type'=>'password'));
        echo $this->Form->input('is_active', array('type' => 'select', 'options' => array('Y' => 'Yes', 'N' => 'No')));

        echo $this->Form->input('created_on', array('style' => 'width:8%', 'default' => date('Y-m-d H:i:s')));
        echo $this->Form->input('update_date', array('style' => 'width:8%', 'default' => date('Y-m-d H:i:s')));
		echo $this->Form->input('role_id',array('label'=>'Role','type'=>'select','default'=>$role,'options'=>$user_role));
        echo $this->Form->input('address');
        echo $this->Form->input('email_id',array('type'=>'text'));
        echo $this->Form->input('mbbs_year');
        echo $this->Form->input('college',array('type'=>'text'));
        


	?>
	</fieldset>
	<div class="submit">
             <?php echo $this->Form->submit(__('Submit', true), array('name' => 'ok', 'div' => false));?>
             <?php echo $this->Form->submit(__('Cancel', true), array('name' => 'cancel','div' => false)); ?>
         </div>
</div>