
<?php
echo $this->Session->flash('auth');
echo $this->Form->create(null, array('url' => array('controller' => 'login', 'action' => 'login')));
echo $this->Form->inputs(array(
	'legend' => __('Login', true),
	'login_id' => array('label' => 'Username','type' => 'text'),
	'password' => array('label' => 'Password','type' => 'password')
));
echo $this->Form->end('Login');

?>
