<?php
    echo $this->Html->css('trc_style');
?>


<script type="text/javascript">
    var val;
    $(document).on('change', 'select', function(e) {
            if(e.target.id=='CdrDistrictId')
            {
                $.ajax({
                        url:'<?php echo $this->webroot; ?>Home/getLocation/',
                        type:'POST',
                        data:{district_id:$(this).val(),type:'thana'},
                        success:function(data){
                            console.log(data);
                            $('#thanas').html(data);
                        },
                        error:function(data){
                            console.log(data.responseText);
                        }

                    });
            }
            else if(e.target.id=='CdrThanaId')
            {
                $.ajax({
                       url:'<?php echo $this->webroot; ?>Home/getLocation/',
                       type:'POST',
                       data:{thana_id:$(this).val(),type:'union'},
                       success:function(data){
                           console.log(data);
                           $('#unions').html(data);
                       },
                       error:function(data){
                           console.log(data.responseText);
                       }

                   });
            }
    });
    $(document).on('change', '#CdrDistrictId', function() {
        console.log($(this).val());

    });
   $(document).on('change', '#CdrThanaId', function() {
           console.log($(this).val());


     });

    $( document ).ready(function(){
            $("#CdrReferredToSpecialist").click(function(){
                                    document.getElementById('CdrSpecialistType').disabled = false;

                 });
                $("#CdrReferredToGeneralPractitioner").click(function(){
                        document.getElementById('CdrSpecialistType').disabled = true;

                  });
                $("#CdrReferredToHospital").click(function(){
                        document.getElementById('CdrSpecialistType').disabled = true;

                  });
                $("#CdrReferredToAdvise").click(function(){
                              document.getElementById('CdrSpecialistType').disabled = true;

                  });

            document.getElementById('CdrSpecialistType').disabled = true;

            $('[name="call_region"]').change(function(){
                if($(this).is(':checked')&&$(this).val()=="Not Defined"){
                    $("#region_div").css("display","none");
                    $("#CdrDistrictId").val("");
                    $("#CdrThanaId").val("");
                    $("#CdrUnionId").val("");
                }else{
                    $("#region_div").css("display","block");
                }
            })

     });

</script>
<div class="services index">

	<table cellpadding="0" cellspacing="0">
    <tr>

    			<th><?php echo $this->Paginator->sort('Phone Number','msisdn');?></th>
    			<th><?php echo $this->Paginator->sort('Call Date','call_date');?></th>
    			<th><?php echo $this->Paginator->sort('Doctor','doctor_id');?></th>

    			<th><?php echo $this->Paginator->sort('ID','doctor_id');?></th>
    			<th><?php echo $this->Paginator->sort('Provisional Diag.','prv_diagnosis_id');?></th>
    			<th><?php echo $this->Paginator->sort('Service Given','service_type');?></th>

    			<th><?php echo $this->Paginator->sort('Chief Complain','cheif_complain');?></th>
    			<th><?php echo $this->Paginator->sort('Referred To','referred_to');?></th>
    			<th><?php echo $this->Paginator->sort('Prescribed Medication','drug1');?></th>
    	</tr>
	<?php
	$i = 0;
	foreach ($caller_history as $user):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>

		<td><?php echo $user['Cdr']['msisdn']; ?>&nbsp;</td>
		<td><?php echo $user['Cdr']['call_date']; ?>&nbsp;</td>
		<td><?php echo $user['User']['name']; ?>&nbsp;</td>

		<td><?php echo $user['Cdr']['doctor_id'];?></td>
        <td><?php echo $user['ProvisionalDiagnosis']['disease_name'];?></td>
        <td><?php echo $user['Cdr']['service_type'];?></td>

		<td><?php echo $user['Cdr']['cheif_complain']; ?>&nbsp;</td>
		<td><?php echo $user['Cdr']['referred_to']; ?>&nbsp;</td>
		<td><?php echo $user['Drug1']['name'].','.$user['Drug2']['name'].','.$user['Drug3']['name']; ?>&nbsp;</td>



	</tr>
<?php endforeach; ?>
	</table>
    <div class="paging">
    		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
    	 | 	<?php echo $this->Paginator->numbers();?>
     |
    		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
    	</div>


</div>
<form action="/trc/Home/add" class="cdr-form" id="CdrHomeForm" method="post" accept-charset="utf-8"><div style="display:none;"><input type="hidden" name="_method" value="POST"></div>

		<legend><h2>Enter Call Detail Records</h2></legend>
  			<div class="simon-1">
               <table border="0">
                <tr>
                    <td style="width:110px!important;">
                        <input type="hidden" name="doctor_id" value=<?php echo $user_id?> id="CdrDoctorId">
                        <div class="input text required"><label for="CdrMsisdn">Msisdn</label>
                    </td>
        
                    <td>
                        <input name="msisdn" type="text" maxlength="20" id="CdrMsisdn" value= <?php echo $cli?>>
                    </td>
                </tr>
                  
                </table>
                <div style="clear:both;"></div>
     	</div>
         
         
         
         <div class="simon-1 simon-2">
          <table>
            
            <tr>
                <td style="width:115px!important;">
                    <label for="CdrCallType">Call Type</label>                
                 </td>
                <td  style="width:150px!important;">

                	<input type="radio" name="call_type" id="CdrCallTypeMedical" value="Medical" checked="checked">
                	<label for="CdrCallTypeMedical">Medical</label>
                </td>
                 
                <td style="width:150px!important;">

                 	<input type="radio" name="call_type" id="CdrCallTypeNonMedical" value="Non Medical">
                 	<label for="CdrCallTypeNonMedical">Non Medical</label>
                </td>
                    
                <td>

                	<input type="radio" name="call_type" id="CdrCallTypeFalseCall" value="False Call">
                	<label for="CdrCallTypeFalseCall">False Call</label>
                </td>
                <td>&nbsp;</td>
            </tr>
		</table>
         <div style="clear:both;"></div>
	</div>
    
		 <div class="simon-1 simon-2">
            <table border="0">
            	<tr>
                <td >
                 	 <label for="CdrCampaignType">Campaign Type</label>                
                 </td>
                    <td>
                         <input type="radio" name="campaign_type" id="CdrCampaignTypeRegular" value="Regular" checked="checked">
                         <label for="CdrCampaignTypeRegular">Regular</label>                
                     </td>
                    <td>
                        <input type="radio" name="campaign_type" id="CdrCampaignTypeSMS" value="SMS">
                        <label for="CdrCampaignTypeSMS">SMS</label>
                    </td>
                
                   <td>

                        <input type="radio" name="campaign_type" id="CdrCampaignTypeTVC" value="TVC">
                        <label for="CdrCampaignTypeTVC">TVC</label>
                   </td>

            	   <td>

                        <input type="radio" name="campaign_type" id="CdrCampaignTypeNewsPaper" value="News Paper">
                        <label for="CdrCampaignTypeNewsPaper">News Paper</label>
                  </td>
            
            </tr>
        </table>
        <div style="clear:both;"></div>
	</div>
      
      
      
       <div class="simon-1 simon-2" >
        <table>
        	<tr>
                <td style="width:113px!important;">
                    <label for="CdrCallRegion">Call Region</label>               
                  </td>
                  <td  style="width:155px!important;">

                        <input  type="radio" name="call_region" id="CdrCallRegionUrbanArea" value="Urban Area">
                        <label for="CdrCallRegionUrbanArea">Urban Area</label>

                     </td>
                   <td style="width:155px!important;">

                        <input class="input radio required" type="radio" name="call_region" id="CdrCallRegionRuralArea" value="Rural Area">
                        <label for="CdrCallRegionRuralArea">Rural Area</label>

                   </td>
                 <td>

                        <input type="radio" name="call_region" id="CdrCallRegionNotDefined" value="Not Defined">
                        <label for="CdrCallRegionNotDefined">Not Defined</label>
                 </td>
            	</tr>
            </table>
		 <div style="clear:both;"></div>
	</div>
     
     <div id= "region_div">
      <div class="simon-3 simon-2">
              <table>
              	<tr>

                  	<td  style="width:130px; text-align:left; font-size:12px; vertical-align:top">
                  	<div class="input text required"><label>Location:</label></td>

                  	<!--
                  	<td>

      					<label for="CdrDistrictId">District</label>
                      </td>
                      -->
                      <td style="width:135px!important; ">
                            <div class="input select required">
                          	<select name="district_id" id="CdrDistrictId">
                          	<option value="">District</option>
                                  <?php foreach($districts as $id=>$name){?>
                                      <option value=<?php echo $id ?>><?php echo $name ?></option>
                                  <?php } ?>

                              </select>
                              </div>
      					</td>
      				<!--
                      <td >
                      	<label for="CdrThanaId">Thana</label>
                       </td>
                       -->

                       <td style="width:175px!important;">

                        <div class="input select required" id="thanas">
                              <select name="thana_id" id="CdrThanaId">
                                  <option value="">Thana</option>
                                       <!--
                                      <?php foreach($thanas as $id=>$name){?>
                                          <option value=<?php echo $id ?>><?php echo $name ?></option>
                                      <?php } ?>
                                      -->

                              </select>
                        </div>
                        </td>

                         <td>
                         <!--
                             <label for="CdrUnionId">Union</label>

                             -->
                             <div id="unions">
                             <select name="union_id" id="CdrUnionId">
                                                     <option value="">Union</option>
                              </div>
                                                     <!--
                              <?php foreach($unions as $id=>$name){?>
                                  <option value=<?php echo $id ?>><?php echo $name ?></option>
                              <?php } ?>
                              -->

                            </select>
                          </td>
                    </tr>
                  </table>
                  <div style="clear:both"></div>
              </div>
              </div>
            
            
          <div class="simon-1">  
            <table>
            <tr> 
            	<td>
            		<div class="input text required"><label for="CdrAgeYear">Caller Age(Year)</label><input name="age_year" type="text" maxlength="4" id="CdrAgeYear">
                </td>
                 
                  <td>
                  	<div class="input text required"><label for="CdrAgeMonth">Caller Age(Month)</label><input name="age_month" type="text" maxlength="4" id="CdrAgeMonth">
                   </td>
        	</tr>
       </table>
        <div style="clear:both"></div>
        </div>  

         <div class="simon-1 simon-2">  
		<table>
        	<tr>
                <td style="width:115px!important;">
                    <label for="CdrSex">Gender</label>                
                 </td>
                 <td style="width:150px!important;">

                    <input type="radio" name="sex" id="CdrSexF" value="F">
                    <label for="CdrSexF">Female</label>               
                  </td>
                
                  <td>

                        <input type="radio" name="sex" id="CdrSexM" value="M">
                        <label for="CdrSexM">Male</label>                
                   </td>
             
            </tr>
        </table>
         <div style="clear:both"></div>
     </div>   
        
        
      <div  class="simon-4 simon-2">  
         <table>
         	<tr>
            	<td style="width:130px!important;">
					<div class="input select required"><label for="CdrPrvDiagnosisId">Provisional Diagnosis</label>
                </td>
                <td>
                    <select name="prv_diagnosis_id" id="CdrPrvDiagnosisId">
                        <?php foreach($diagnosises as $id=>$name){?>
                                                        <option value=<?php echo $id ?>><?php echo $name ?></option>
                                                    <?php } ?>
                    </select>				
					</td>
               	</tr>
               </table>
                <div style="clear:both"></div>
     	</div>  
               
            
          <div  class="simon-4">    
               <table>
               	<tr> 
					<td style="width:130px!important;">
                    	<label for="CdrCheifComplain">Cheif Complain</label>
                    </td>
                    <td class="txtAra">
                        <textarea name="cheif_complain" legend="false" cols="30" rows="6" id="CdrCheifComplain"></textarea></div>                </td>
              	</tr>
              </table>
               <div style="clear:both"></div>
              </div>
              <div class="simon-1 simon-2">
              		<table>
                      	<tr>
                              <td style="width:115px!important;">
                                  <label for="CdrService">Service Given</label>
                               </td>
                               <td style="width:150px!important;">

                                  <input type="radio" name="service_type" id="CdrServiceM" value="Medicine">
                                  <label for="CdrServiceM">Medicine</label>
                                </td>

                                <td>

                                      <input type="radio" name="service_type" id="CdrServiceA" value="Advice Only">
                                      <label for="CdrServiceA">Advice Only</label>
                                 </td>
                                 <td>

                                      <input type="radio" name="service_type" id="CdrServiceAM" value="Medicine and Advice">
                                      <label for="CdrServiceAM">Medicine and Advice</label>
                                 </td>
                                   <td>

                                      <input type="radio" name="service_type" id="CdrServiceH" value="Health Information">
                                      <label for="CdrServiceH">Health Information</label>
                                 </td>
                          </tr>
                      </table>
                       <div style="clear:both"></div>
                   </div>
              <div class="simon-3 simon-2">
              <table>
              		<tr>
						<td style="width:130px;">
                			<label for="CdrDrug1">Medicine1</label>
                        </td>
                        <td style="width:150px;">
                            	<select name="drug1" id="CdrDrug1">
                            	<option value="">Medicine 1</option>
                                    <?php foreach($drugs as $id=>$name){?>
                                        <option value=<?php echo $id ?>><?php echo $name ?></option>
                                    <?php } ?>
                                </select>               
                         </td>
               			 <td style="width:130px;">
                         	<label for="CdrDrug2">Medicine2</label>
                         </td>
                         <td style="width:150px;">
                            	<select name="drug2" id="CdrDrug2">
                                    <option value="">Medicine 2</option>
                                     <?php foreach($drugs as $id=>$name){?>
                                        <option value=<?php echo $id ?>><?php echo $name ?></option>
                                    <?php } ?>
                               	</select>
                           </td>

                            <td style="width:150px;">

                                <label for="CdrDrug3">Medicine3</label>

                            </td>
                            <td>
                                <select name="drug3" id="CdrDrug3">
                                        <option value="">Medicine 3</option>
                                         <?php foreach($drugs as $id=>$name){?>
                                            <option value=<?php echo $id ?>><?php echo $name ?></option>
                                        <?php } ?>
                                    </select>

                            </td>


        				</tr>
            		</table>
        
        			<div style="clear:both"></div>
     	</div>  
        
        <div class="simon-5 simon-2">
        	<table>
            	<tr>
                 <td style="width:115px!important;">
                    <label for="CdrReferredTo">Referred To</label>               
                 </td>
                 <td style="width:150px!important;">

                    <input type="radio" name="referred_to" id="CdrReferredToGeneralPractitioner" value="General Practitioner" checked="checked">
                    <label for="CdrReferredToGeneralPractitioner">G.Practitioner</label>
                  </td>
                
                  <td style="width:150px!important;">

                    <input type="radio" name="referred_to" id="CdrReferredToHospital" value="Hospital">
                    <label for="CdrReferredToHospital">Hospital</label>
                  </td>                
                  
                  <td>

                        <input type="radio" name="referred_to" id="CdrReferredToSpecialist" value="Specialist">
                        <label for="CdrReferredToSpecialist">Specialist</label>
                   </td>
                    <td>

                       <input type="radio" name="referred_to" id="CdrReferredToAdvise" value="No Referral" checked="checked">
                       <label for="CdrReferredToAdvise">No Referral</label>
                    </td>
           	 </tr>
           </table>
           	<div style="clear:both"></div>
     	</div>  
        
 <script type="text/javascript">

                $("#CdrReferredToSpecialist").click(function(){
                        document.getElementById('CdrSpecialistType').disabled = false;

                 });
                $("#CdrReferredToGeneralPractitioner").click(function(){
                        document.getElementById('CdrSpecialistType').disabled = true;

                  });
                $("#CdrReferredToHospital").click(function(){
                        document.getElementById('CdrSpecialistType').disabled = true;

                  });
                $( document ).ready(function(){

                        document.getElementById('CdrSpecialistType').disabled = true;

                 });

        </script>
		

		<div class="simon-4 simon-2">
        	<table>
            	<tr>
                	<td style="width:130px!important;">
        				<label for="CdrSpecialistType">Specialist Type</label>
                     </td>
                     <td>	
                        	<select name="specialist_type" id="CdrSpecialistType">
                                <option value="">Specialist</option>
                                 <?php foreach($specialists as $id=>$name){?>
                                                                                                                            <option value=<?php echo $id ?>><?php echo $name ?></option>
                                                                                                                        <?php } ?>
						</select>   
                      </td>
                   </tr>
               </table>
               <div style="clear:both"></div>
           </div>  
            
            
            <div class="simon-1 simon-2">             
             <table>
             	<tr>
                     <td style="width:115px!important;">
                        <label for="CdrIsSatisfied">Is Satisfied</label>               
                      </td>
                    
                      <td style="width:150px!important;">

                            <input type="radio" name="is_satisfied" id="CdrIsSatisfiedY" value="Y">
                            <label for="CdrIsSatisfiedY">Yes</label>
                      </td>
                    
                      <td>

                            <input type="radio" name="is_satisfied" id="CdrIsSatisfiedN" value="N">
                            <label for="CdrIsSatisfiedN">No</label></div>               
                       </td>
                 
                	</tr>
                    
                </table>
                <div style="clear:both"></div>
              </div>

				<div class="simon-4">
                	 <table>
             			<tr>
                        	<td style="width:130px!important;">
                				<div><label for="CdrNote">Note</label>
                             </td>
                             <td>
                 				<textarea name="note" options="" cols="30" rows="6" id="CdrNote"></textarea>
                            </td>
                        </tr>
                      </table>
                  <div style="clear:both"></div>
              </div>           

	
	
<div class="submit"><input type="submit" value="Submit"></div></form>