$(document).ready(function(){
    
    $('#ReportHourly').click(function(){
		if( $('#ReportHourly').is(':checked') )
		{
			$('#ReportMonthly').removeAttr('checked') ;
		}
	}) ;
	$('#ReportMonthly').click(function(){
		if( $('#ReportMonthly').is(':checked') )
		{
			$('#ReportHourly').removeAttr('checked') ;
		}
	}) ;
        
    setNavigation();
    $('tr.on_hover').hover(function(e){
		$(this).addClass('hover_color') ;
	}, function(){
		$(this).removeClass('hover_color') ;
	}) ;

});

function setNavigation() {
    var path = window.location.pathname;
	var urlStr = '' ;var href = '' ;var this_link = '' ;
    path = path.replace(/\/$/, "").replace('_', '') ;
	
    path = decodeURIComponent(path);
    //console.log(path);
    $("#leftPnael a").each(function () {
        href = $(this).attr('href');
        if(href){           
            urlStr = path.substring(path.indexOf('/'), href.length).toLowerCase();
            //console.log(urlStr+'--'+href);
            if (urlStr == href.toLowerCase()) {
				if(this_link){
					this_link.closest('li').removeClass('li_selected');
				}
				this_link = $(this) ;
                $(this).closest('li').addClass('li_selected');
				return ;
            }
        }
    });
}